var canvas = document.getElementById('canv'), //canvas element
	ctx = canvas.getContext('2d'),
	doc = $(document), //document for events
	isKeyDown = false, // flag for checking mouse status
	settings = {

		lineColor: 'red',
		lineWidth: 10,
		mode: 'free',
		fieldBg: function () {
			return $(canvas).css('background-color');
		}

	};

//adding sizes of canvas block
canvas.width = window.innerWidth;
canvas.height = window.innerHeight - 100;

$(window).on('resize', function () {
	
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight - 100;

});

//navigation elems

var widthInput = $('#width_line'),
	colorSelectors = $('.color'),
	clearBttn = $('#clear');

//functions for painting
var freePaint = function () {



	},

	clearField = function () {
		ctx.fillStyle = settings.fieldBg();
		ctx.fillRect(0, 0, canv.width, canv.height);

		ctx.beginPath();
		ctx.fillStyle = settings.lineColor;
	},

	changeColor = function () {
		
		settings.lineColor = $(this).attr('id');

		ctx.strokeStyle = settings.lineColor;
		ctx.fillStyle = settings.lineColor;

		colorSelectors.removeClass('active');
		$(this).addClass('active');

	}

//special events

//documet events
doc.on('mousedown', function () {
	isKeyDown = true;
});

doc.on('mouseup', function () {
	isKeyDown = false;
});

doc.on('mousemove', function () {

	//painting modes
	if (isKeyDown) {
		freePaint();
	}

});

//nav events

widthInput.on('change', function () {
	
	settings.lineWidth = Number($(this).val());

});

colorSelectors.on('click', function () {

	changeColor();

});

clearBttn.on('click', function () {
	
	clearField();

});